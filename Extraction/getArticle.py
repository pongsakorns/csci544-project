#!/usr/bin/python

from cgi import valid_boundary
from collections import defaultdict
import sys,os,glob
import codecs
import random

topicPath = str(sys.argv[1])
wikiPath = str(sys.argv[2])

topicfiles = os.listdir('./%s/'%topicPath)
wikifiles = os.listdir('./%s/'%wikiPath)


wikiFullPath = []
topic ={}
def readWikiPath():
    for fidx in range(0,len(wikifiles)):
        folderName = wikifiles[fidx]
        fileName    = os.listdir('./%s/%s/'%(wikiPath,folderName))
        for fileidx in range(0,len(fileName)):
            fpath =  wikiPath+'/'+folderName+'/'+fileName[fileidx]
            wikiFullPath.append(fpath)


        #
        # f = open(fpath,"r",errors='ignore')
        # array = f.read().split('\n')
        # print("document>>",fidx)

def readTopic():
    for fidx in range(0,len(topicfiles)):
        fileName  = topicfiles[fidx]
        fpath =  topicPath+'/'+fileName
        print(fpath)
        f = open(fpath,"r",errors='ignore')
        array = f.read().split('\n')

        array = [item.lower() for item in array]

        if '' in array:
            array.remove('')
        topic.setdefault(fileName,array)

def getArticle(article):
    for aidx in range(0,len(article)):
        lines = article[aidx].split('\n')
        while '' in lines:
            lines.remove('')
        article_name = lines[1]
        article_name = article_name.lower()

        for key,valuelist in topic.items():
            if article_name in valuelist:
                if len(lines)>5:
                    writeSelectArticle(lines,key,article_name)
                    #print('country>>>>>>>>>',article_name,key)
        print(article_name)

def writeSelectArticle(lines,catagory,article_name):
    folderpath = 'selectedArticle'+'/'+catagory
    if not os.path.exists(folderpath): os.makedirs(folderpath)
    fpath = 'selectedArticle'+'/'+catagory+'/'+article_name
    try:
        f = open(fpath,"w",errors='ignore')
        for lineidx in range(1,len(lines)):
            if lineidx == 2:
                f.write(catagory+'\n')
            f.write(lines[lineidx]+'\n')
    except OSError:
        return
    # f.write('</doc>')


def readWiki():
    for widx in range(0,len(wikiFullPath)):
        fpath  = wikiFullPath[widx]
        f = open(fpath,"r",errors='ignore')
        article = f.read().split('</doc>')
        while '' in article:
            article.remove('')
        del article[-1]
        getArticle(article)



# readWikiPath()
# print(topicPath)
readWikiPath()
readTopic()
readWiki()
