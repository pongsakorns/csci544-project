Brown rice
food
Brown rice (or "hulled" or "unmilled" rice) is whole grain rice. It has a mild, nutty flavor, and is chewier and more nutritious than white rice, but goes rancid more quickly because the bran and germ—which are removed to make white rice—contain fats that can spoil. Any rice, including long-grain, short-grain, or glutinous rice, may be eaten as brown rice.
White rice comparison.
Brown rice and white rice have similar amounts of calories and carbohydrates. The main differences between the two forms of rice lie in processing and nutritional content.
When only the outermost layer of a grain of rice (the husk) is removed, brown rice is produced. To produce white rice, the next layers underneath the husk (the bran layer and the germ) are removed, leaving mostly the starchy endosperm.
Several vitamins and dietary minerals are lost in this removal and the subsequent polishing process. A part of these missing nutrients, such as vitamin B1, vitamin B3, and iron are sometimes added back into the white rice making it "enriched", as food suppliers in the US are required to do by the Food and Drug Administration.
One mineral not added back into white rice is magnesium; one cup (195 g) of cooked long grain brown rice contains 84 mg of magnesium while one cup of white rice contains 19 mg.
When the bran layer is removed to make white rice, the oil in the bran is also removed. Rice bran oil may help lower LDL cholesterol.
Among other key sources of nutrition lost are dietary fiber and small amounts of fatty acids.
Preparation.
A nutritionally superior method of preparation using GABA rice or germinated brown rice (GBR) (also known as Hatsuga genmai in Japan), developed during the International Year of Rice, may be used. This involves soaking washed brown rice for 20 hours in warm water (34 °C or 93 °F) prior to cooking it. This process stimulates germination, which activates various enzymes in the rice. By this method, it is possible to obtain a more complete amino acid profile, including GABA.
Storage.
Brown rice has a shelf life of approximately 6 months, but hermetic storage, refrigeration or freezing can significantly extend its lifetime. Freezing, even periodically, can also help control infestations of Indian meal moths.
